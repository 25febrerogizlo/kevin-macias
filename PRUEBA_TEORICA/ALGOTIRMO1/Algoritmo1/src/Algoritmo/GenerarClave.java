package Algoritmo;

import java.util.Random;

public class GenerarClave {
	//variables globales
	String chars = "@-=+./$";
	String min= "abcdefghijklmnopqrstuvwxyz";
	String nums= "0123456789";
	String words="ABCDEFGHIJKLMNOPQRSTUVWXYZ";	
	public String generar() {
		Random aleatorio = new Random(System.currentTimeMillis());
		int nivel=0;
	//ciclamos hasta que la clave tenga un tama�o  de 8 y 15 
		while(nivel <8 || nivel >15) {
			nivel = aleatorio.nextInt(16);
		}
		System.out.println(nivel);
		// obtenemos el porcentaje de cada parametro
		double porc = nivel /4;
		String clave = ""; 
		// generamos la clave 
		for(int i=0;i<porc;i++) {
			clave = clave + chars.charAt(aleatorio.nextInt(chars.length()));
			clave = clave + min.charAt(aleatorio.nextInt(min.length()));
			clave = clave + nums.charAt(aleatorio.nextInt(nums.length()));
			clave = clave + words.charAt(aleatorio.nextInt(words.length()));
		}
		return clave;
	}
}
