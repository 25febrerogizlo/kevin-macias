package com.multiplos.cuentas.services;

import java.util.List;

import com.multiplos.cuentas.models.Producto;

public interface TiendaService {

	
	
	List <?>ListarProductos()throws Exception;
	
	Object putProducto(Producto nuevo)throws Exception;
	
}
