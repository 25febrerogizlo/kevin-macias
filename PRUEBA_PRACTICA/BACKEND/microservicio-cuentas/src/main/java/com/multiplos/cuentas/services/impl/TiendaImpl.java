package com.multiplos.cuentas.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.multiplos.cuentas.models.Producto;
import com.multiplos.cuentas.repository.TiendaRepository;
import com.multiplos.cuentas.services.TiendaService;

@Service
public class TiendaImpl implements TiendaService {

	private TiendaRepository tiendaRepo;
	
	@Autowired
	public TiendaImpl(TiendaRepository tiendaRepo) {
		this.tiendaRepo=tiendaRepo;
	}
	
	
	@Override
	public List<?> ListarProductos() throws Exception {
		
		return this.tiendaRepo.findAll();
	}

	@Override
	public Object putProducto(Producto producto) throws Exception {
		Producto nuevoProducto;
		Object response;
		if(producto.getId()!=null){
			Optional<Producto> op=	this.tiendaRepo.findById(producto.getId());
			if(op.isEmpty()) {
				throw new Exception ("Producto con id: "+ producto.getId()+" no existe");
			}
			nuevoProducto= op.get();
			response="producto agregado correctamenet";
		}else {
			
			nuevoProducto = producto.clone();
			response="producto actualizado correctamente";
		}
		this.tiendaRepo.save(nuevoProducto);
		return response;
	}

}
