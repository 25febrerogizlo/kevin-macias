package com.multiplos.cuentas.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "carrito", schema = "cuenta")
public class carrito implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "codigo")
	private String codigo;
	
//	@JsonIgnoreProperties(value = {"idCarrito"}, allowSetters = true)
//	@OneToMany(mappedBy = "idCarrito" ,fetch = FetchType.LAZY,orphanRemoval = true,cascade = CascadeType.ALL)
//	private List<CarritoDetalle> detalles;
	
}
