package com.multiplos.cuentas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.multiplos.cuentas.models.User;

public interface CuentaRepository extends JpaRepository<User, Long>{

	
	@Query("select u from User u where u.usser = :usser")
	User findByUsser(String usser);
}
