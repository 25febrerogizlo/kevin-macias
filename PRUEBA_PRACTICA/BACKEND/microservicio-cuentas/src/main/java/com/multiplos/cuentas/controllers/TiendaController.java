package com.multiplos.cuentas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.multiplos.cuentas.models.BadResponse;
import com.multiplos.cuentas.models.Producto;
import com.multiplos.cuentas.models.Response;
import com.multiplos.cuentas.services.CuentaService;
import com.multiplos.cuentas.services.TiendaService;

@RestController
public class TiendaController {

	private TiendaService tiendaService;
	private CuentaService cuentaService;

	@Autowired
	public TiendaController(TiendaService tiendaService, CuentaService cuentaService) {
		this.tiendaService = tiendaService;
		this.cuentaService = cuentaService;
	}

	@PostMapping("public/logers")
	public ResponseEntity<?> login(@RequestParam String user, @RequestParam String pass) throws Exception {
		Object response;
		try {
			response = this.cuentaService.login(user, pass);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new BadResponse(e.getMessage()));
		}
		return ResponseEntity.ok().body(new Response(response));
	}

	@PostMapping("public/listar/productos")
	public ResponseEntity<?> ListarProductos(@RequestParam String text) throws Exception {
		List<?> productos;
		try {
			productos = tiendaService.ListarProductos();
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new BadResponse(e.getMessage()));
		}
		return ResponseEntity.ok(productos);
	}

	@PutMapping("admin/put/productos")
	public ResponseEntity<?> PutProductos(@RequestBody Producto producto) throws Exception {
		Object response;
		try {
			response = tiendaService.putProducto(producto);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(new BadResponse(e.getMessage()));
		}
		return ResponseEntity.ok().body(new Response(response));
	}

}
