package com.multiplos.cuentas.services.impl;

import org.springframework.stereotype.Service;

import com.multiplos.cuentas.models.User;
import com.multiplos.cuentas.repository.CuentaRepository;
import com.multiplos.cuentas.services.CuentaService;

@Service
public class CuentaServiceImpl implements CuentaService {

	private CuentaRepository cuentaRepo;

	public CuentaServiceImpl(CuentaRepository cuentaRepo) {
		this.cuentaRepo = cuentaRepo;
	}

	@Override
	public Object login(String usser, String pass) throws Exception {

		User userLog = this.cuentaRepo.findByUsser(usser);
		if (userLog == null) {
			throw new Exception("Usuario: " + usser + " no existe");
		}
		if(!userLog.getPass().equals(pass)) {
			throw new Exception("Clave incorrecta ");
		}
		return "true";
	}

}
