package com.multiplos.cuentas.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;

import com.fasterxml.jackson.annotation.JsonRawValue;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "productos", schema = "cuenta")
public class Producto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "price", precision = 12, scale = 2 )
	private BigDecimal price;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "image")
	private String image;
	
	@ColumnTransformer(write = "?::jsonb")
	@Column(name = "rating", columnDefinition = "json")
	@JsonRawValue
	private String rating;
	
	@Column(name = "activo")
	private Boolean activo;
	
	public Producto (Producto target) {
		this.activo=target.activo;
		this.description=target.description;
		this.image=target.image;
		this.title=target.title;
		this.price=target.price;
		this.rating=target.rating;
	}

	
	public Producto clone() {
		return new Producto(this) ;
	}
	
	
	
}
