import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListUserComponent } from './modules/users/components/list-user/list-user.component';
import { UsersModule } from './modules/users/users.module';
import { NavbarComponent } from './componentsGlobals/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    ListUserComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UsersModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
